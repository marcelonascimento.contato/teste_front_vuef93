const states = {
    'on_time': 'dentro do prazo',
    'paid': 'pago',
    'overdue': 'em atraso'
}

const getStatusLabel = ((state) => {    
    return states[state]
});

module.exports = {
    getStatusLabel,
    states
}