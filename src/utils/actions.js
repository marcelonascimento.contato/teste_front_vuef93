const actions = {
    'thank_payment': 'agradecer pagamento',
    'cancel_contract': 'cancelar contrato',
    'bill': 'cobrar'
}

const getActionLabel = ((action) => {    
    return actions[action]
});

module.exports = {
    getActionLabel,
    actions
}