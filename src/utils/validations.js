import { isCpf } from '../utils/validators';
import { maxLength, integer, numeric, required, minLength } from 'vuelidate/lib/validators';

class Validations {
    create = {
        model: {
          name: {
            required,
            minLength: minLength(4),
            maxLength: maxLength(99),
          },
          phone: {
            required,
            minLength: minLength(13),
          },
          document: {
            required,
            minLength: minLength(4),
            isCpf
          },
          contractNumber: {
            required,
            numeric,
            integer
          },
          contractValue: {
            required,
            minLength: minLength(4),
            numeric,
          },
          contractStatus: {
            required,
            minLength: minLength(4),
          },
          contractDate: {
            required,
            minLength: minLength(10),
          },
          action: {
            required,
            minLength: minLength(4),
          },
        }
    }
}

export default new Validations();

