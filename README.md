# Sobre o projeto

Este é um projeto em VueJS.

##### Documentação da API: http://localhost:3000/api/doc (Disponivel apenas quando subir a aplicação)

## O Desafio

#### Requisitos

- Criar uma API Rest para realizar as seguintes ações:
    - Gerenciar clientes (CRUD) e listar clientes com filtros de status
<br>

## A solução

#### Como rodar o projeto

**Antes de seguir os passos abaixo tenha certeza que a [API](https://gitlab.com/marcelonascimento.contato/teste_node_f104) esta funcionando corretamente. Para rodar este projeto:**

1\. Clone este repositorio  e entre na pasta

```
git clone https://gitlab.com/marcelonascimento.contato/teste_front_vuef93.git

cd teste_front_vuef93
```

2\. Instale as dependências necessárias.

```
npm install
```

3\. Inicie o projeto

```
npm run dev
```

4\. Agora você pode acessar aplicação em [localhost ou clique aqui!](http://localhost:8080)
